class Table {

    static matrix() {
        erase();
        rect(0  , 0, 200, 600);
        noErase();

        fill('#006600');
        square(300, 100, 600);

        fill('#663300');
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 9; j++) {
                if (a[i][j] != 0)
                    square(380 + i * 40 + i * 10, 180 + j * 40 + j * 10, 40);
            }
        }
    }

    static wallSpace() {

        fill("black");
        rect(370, 165, 460, 10);
        rect(370, 625, 460, 10);

        fill('gray');
        for (let i = 0; i < 9; i++) {
            rect(380 + i * 40 + i * 10, 105, 40, 60);
            rect(380 + i * 40 + i * 10, 635, 40, 60);


        }

    }


    //new
    static walls() {

        //zidurile de sus
        fill('white');
        for (let i = 0; i < 10; i++)
            if (walls1[i] != 0) {
                rect(370 + i * 40 + i * 10, 100, 10, 65);
            }

        //zidurile de jos
        fill('white');
        for (let i = 0; i < 10; i++)
            if (walls2[i] != 0) {
                rect(370 + i * 40 + i * 10, 635, 10, 65);
            }
          
     }
} 