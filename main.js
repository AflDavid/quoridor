var a = [
    [1, 1, 1, 1, 2, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 3, 1, 1, 1, 1],
];
let playerName1 = "";
let playerName2 = "";


var xUser = 600;
var yUser = 600;

var xComp = 600;
var yComp = 200;
var walls1 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
var walls2 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
var inputPlayer1;
var inputPlayer2;
var moveRed = false;
var Player1;
var moveBlue = false;
var Player2;
var roundPlayer = 1;
var vWall = false;
var hWall = false;
var wall1=9;
var wall2=9;

var button;

function setup() {
    //canvas
    createCanvas(1000, 1000);
  

    buttonReset = createButton('Restart');
    buttonReset.position(1050, 700);
    buttonReset.mousePressed(resetGame);
    buttonReset.size(100);
  
    buttonMoveR = createButton('move Red');
    buttonMoveR.position(1050, 30);
    buttonMoveR.size(100);
    buttonMoveR.mousePressed(White);
    
    buttonMoveB = createButton('move Blue');
    buttonMoveB.position(1050, 470);
    buttonMoveB.size(100);
    buttonMoveB.mousePressed(Black);

    buttonVerticalWallA = createButton('vertical Wall');
    buttonVerticalWallA.position(1050, 60);
    buttonVerticalWallA.size(100);
    buttonVerticalWallA.mousePressed(vWall1);

    buttonHorizontalWallA = createButton('horizontal Wall');
    buttonHorizontalWallA.position(1050, 90);
    buttonHorizontalWallA.size(100);
    buttonHorizontalWallA.mousePressed(hWall1);

    buttonVerticalWallB = createButton('vertical Wall');
    buttonVerticalWallB.position(1050, 500);
    buttonVerticalWallB.size(100);
    buttonVerticalWallB.mousePressed(vWall2);

    buttonHorizontalWallB = createButton('horizontal Wall');
    buttonHorizontalWallB.position(1050, 530);
    buttonHorizontalWallB.size(100);
    buttonHorizontalWallB.mousePressed(hWall2);

  

    buttonCreate1 = createButton('Add First Player');
    buttonCreate1.position(20, 50);
    buttonCreate1.size(135, 60);
    buttonCreate1.style('background-color', this.buttonColor);
    buttonCreate1.mousePressed(displayPlayer1);

    buttonCreate2 = createButton('Add Second Player');
    buttonCreate2.position(20, 150);
    buttonCreate2.size(135, 60);
    buttonCreate2.style('background-color', this.buttonColor);
    buttonCreate2.mousePressed(displayPlayer2);


}

function displayPlayer1() {
    input1 = createInput("Player 1");
    input1.position(20, 300);
    Player1 = input1.value();

}

function displayPlayer2() {
    input2 = createInput("Player 2");
    input2.position(20, 350);
    Player2 = input2.value();

}

function draw(){
    insert();
    buttonReset.mousePressed(resetGame);
    buttonStart.mousePressed(start);
    text(playerName1 , 20, 180);
    text(playerName2, 20, 220);
}

function resetGame(){
    //resetare pozitii pioni
    xUser = 600;
    yUser = 600;
    xComp = 600;
    yComp = 200;

    //sterge numele
    playerName1 = "";
    playerName2 = "";

    a = [
        [1, 0, 1, 0, 1, 0, 1, 0, 3, 0, 1, 0, 1, 0, 1, 0, 1, -3],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
        [1, 0, 1, 0, 1, 0, 1, 0, 4, 0, 1, 0, 1, 0, 1, 0, 1, -3],
        [-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3],
        ];
}

//new

function insert() {
    Table.matrix();
    Table.wallSpace();
    Table.walls();

    pawn1(xComp, yComp);
    pawn2(xUser, yUser);

    if (vWall) {
        fill('grey');
        rect(mouseX - 45, mouseY, 90, 10);
    }
    if (hWall) {
        fill('grey');
        rect(mouseX, mouseY - 45, 10, 90);
    }
    fill('grey');
    for (let j = 0; j < 17; j++)
        for (let i = 0; i < 17; i++) {
            if (a[i][j] == -1)
                if (i % 2 == 1 && j % 2 == 1)
                    rect(263 + j * 25, 95 + i * 25, 90, 10);
            if (a[i][j] == -2)
                if (i % 2 == 1 && j % 2 == 1)
                    rect(303 + j * 25, 55 + i * 25, 10, 90);
        }
    text(roundPlayer,10,200);
}


function White() {
    if (roundPlayer == 1) {
        hWall = false;
        vWall = false;
        moveRed = true;
        moveBlue = false;
    }
}

function Black() {
    if (roundPlayer == 2) {
        hWall = false;
        vWall = false;
        moveBlue = true;
        moveRed = false;
    }
}

function vWall1() {
    if (roundPlayer == 1) {
        hWall = false;
        vWall = true;
        moveRed = false;
        moveBlue = false;
    }
}

function vWall2() {
    if (roundPlayer == 2) {
        hWall = false;
        vWall = true;
        moveRed = false;
        moveBlue = false;
    }
}

function hWall1() {
    if (roundPlayer == 1) {
        vWall = false;
        hWall = true;
        moveRed = false;
        moveBlue = false;
    }
}

function hWall2() {
    if (roundPlayer == 2) {
        vWall = false;
        hWall = true;
        moveRed = false;
        moveBlue = false;
    }
}